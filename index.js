const {ApolloServer, gql} = require('apollo-server')
const { RESTDataSource } = require('apollo-datasource-rest')
// const fetch = require('node-fetch')

const typeDefs = gql`
  type Category {
    name: String
  }
  
  type Joke {
    created_at: String
    icon_url: String
    id: String
    updated_at: String
    url: String
    value: String
  }
  
  type Query {
    categories: [Category]
    joke(category: String): Joke
  }`



class ChuckNorrisAPI extends RESTDataSource {
    constructor() {
        super()
        this.baseURL = 'https://api.chucknorris.io/jokes/'
    }

    async getCategories() {
        return this.get(`categories`)
    }

    async getJoke( category ) {
        return this.get('random', {
            category: category
        })
    }
}

const resolvers = {
    Query: {
        /**
         *
         * @property categories
         * @returns {Array<String>}
         * @example
         * { categories { name } }
         *
         */
        categories: async (_source, {}, { dataSources }) => {
            const categories = await dataSources.ChuckNorrisAPI.getCategories()
            return categories.map( name => ({name}))
        },

        /**
         *
         * @property joke
         * @returns {Object<String>}
         * @example
         * { joke(category: "dev"){ id value } }
         *
         */
        joke: async (_source, { category }, { dataSources }) => {
            return dataSources.ChuckNorrisAPI.getJoke(category)
        }
    }
}

const server = new ApolloServer(
    {
        typeDefs,
        resolvers,
        dataSources: () => {
            return {
                ChuckNorrisAPI: new ChuckNorrisAPI(),
            }
        },
        // context: () => {
        //     return {
        //         token: 'foo',
        //     };
        // },
    }
)

server.listen().then(({url}) => {
    console.log(`Server ready at ${url}`)
})
